import { DocumentBuilder } from '@nestjs/swagger';

export const SWAGGER_CONFIG = new DocumentBuilder()
  .setTitle('Calmira Linux web services api')
  .setDescription('Calmira Linux web services public api')
  .setVersion('1.0')
  .build();
