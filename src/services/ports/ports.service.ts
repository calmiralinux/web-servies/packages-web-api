import { Injectable } from '@nestjs/common';
import { AxiosHttpClient } from '../axios-http.client';
import { mapToPortBase } from './mapper';
import { PortInfoItem, PortMetaData } from './models/port-info-item';

export const DEFAULT_BRANCH = 'testing';

@Injectable()
export class PortsService {
  constructor(private readonly axiosHttpClient: AxiosHttpClient) {}

  private get urlBase() {
    return 'https://gitlab.com/calmiralinux/cabs/Ports/-/raw';
  }

  public async getPortInfo(
    branch: string = DEFAULT_BRANCH,
    port: string,
  ): Promise<PortInfoItem> {
    const url = `${this.urlBase}/${branch}/ports/${port}/port.toml`;

    const portInfo = await this.axiosHttpClient.get<string>(url);
    const data = this.parseToml<PortInfoItem>(portInfo);

    return { ...data, package: { ...data.package, branch, fullName: port } };
  }

  public async getPortInstallInfo(
    branch: string = DEFAULT_BRANCH,
    port: string,
  ) {
    const url = `${this.urlBase}/${branch}/ports/${port}/install`;

    return await this.axiosHttpClient.get(url, 'text');
  }

  public async getPortReadme(branch: string = DEFAULT_BRANCH, port: string) {
    const url = `${this.urlBase}/${branch}/ports/${port}/README.md`;

    return await this.axiosHttpClient.get(url, 'text');
  }

  public async getMetadata(
    branch: string = DEFAULT_BRANCH,
  ): Promise<PortMetaData> {
    const url = `${this.urlBase}/${branch}/ports/metadata.toml`;

    const portsMetadata = await this.axiosHttpClient.get<string>(url);
    return this.parseToml<PortMetaData>(portsMetadata);
  }

  public async getPorts(
    branch: string = DEFAULT_BRANCH,
  ): Promise<PortInfoItem[]> {
    const portBaseList = mapToPortBase(
      (await this.getMetadata(branch)).port_sys.ports ?? [],
    );

    return await Promise.all(
      portBaseList.map(
        async (port) => await this.getPortInfo(branch, port.fullName),
      ),
    );
  }

  private parseToml<T>(data: string): T {
    // eslint-disable-next-line @typescript-eslint/no-var-requires
    const TOML = require('fast-toml');
    return <T>(TOML.parse(data) as unknown);
  }
}
