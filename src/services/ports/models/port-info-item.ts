export interface PortInfoItem {
  package: {
    fullName: string;
    name: string;
    version: string;
    description: string;
    branch: string;
    maintainer: string;
    releases: string[];
    priority: string;
    usage: number;
    upgrade_mode: string;
    build_time: string;
  };
  deps: {
    required: string[];
  };
  port: {
    url: string;
    md5: string;
    sha256: string;
  };
}

export interface PortBase {
  name: string;
  branch: string;
  fullName: string;
  version: string;
  modified: Date;
}

export interface PortInfoMetaData {
  name: string;
  version: string;
  modified: Date;
}

export interface PortMetaData {
  port_sys: {
    categories: string[];
    ports: PortInfoMetaData[];
  };
  system: {
    release: string[];
  };
}
