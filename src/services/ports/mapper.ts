import { PortBase, PortInfoMetaData } from './models/port-info-item';

export const EXCLUDED_PORTS = ['base/sysvinit'];

export const mapToPortBase = (ports: PortInfoMetaData[]): PortBase[] =>
  ports
    ?.filter((item) => !!item && !EXCLUDED_PORTS.includes(item.name))
    .map((port) => {
      const portArray = port.name.split('/');
      let portArrayLength = portArray.length;

      const name = portArray[--portArrayLength];
      let branch = portArray[0];

      if (portArrayLength > 2) branch = `${branch}${portArray[1]}`;

      return {
        name,
        branch,
        fullName: port.name,
        version: port.version,
        modified: port.modified,
      };
    });
