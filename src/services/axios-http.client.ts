import { Injectable, Logger } from '@nestjs/common';
import axios, { ResponseType } from 'axios';

@Injectable()
export class AxiosHttpClient {
  private readonly logger = new Logger(AxiosHttpClient.name);

  public async get<T>(url: string, responseType: ResponseType = 'blob') {
    this.logger.log(`AxiosHttpClient request. Method [GET], Url: [${url}]`);
    let res = <T>{};

    try {
      res = <T>(
        await axios({
          url,
          method: 'GET',
          responseType: responseType,
        })
      ).data;
    } catch (error) {
      this.logger.error(`Error from request url: [${url}]`);
    }

    return res;
  }
}
