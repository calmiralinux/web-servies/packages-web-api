/** Модель ответа ping. */
export interface PingResponseModel {
  /** Ответ. */
  response: string;
  /** Дата. */
  date: Date;
}
