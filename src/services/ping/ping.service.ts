import { Injectable } from '@nestjs/common';
import { PING_PONG_RESPONSE } from '../../constants';
import { PingResponseModel } from './models/ping.model';

/**
 * Ping сервис.
 * */
@Injectable()
export class PingService {
  /** pong. */
  public pong = (): PingResponseModel => ({
    response: PING_PONG_RESPONSE,
    date: new Date(),
  });
}
