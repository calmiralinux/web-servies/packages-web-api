export const USER_LOGGER = { logger: true, bufferLogs: true };
export const PING_PONG_RESPONSE = 'PONG';

export const APP_FALLBACK_PORT = 3000;

export const CONFIG_KEYS = {
  APP_PORT: 'APP_PORT',
};
