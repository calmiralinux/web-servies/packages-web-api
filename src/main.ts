import { Logger, VersioningType } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import {
  FastifyAdapter,
  NestFastifyApplication,
} from '@nestjs/platform-fastify';
import { SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';
import configuration from './config';
import { USER_LOGGER } from './constants';
import { MESSAGES } from './messages';
import { SWAGGER_CONFIG } from './providers/swagger';

async function bootstrap() {
  const app = await NestFactory.create<NestFastifyApplication>(
    AppModule,
    new FastifyAdapter(USER_LOGGER),
  );

  app.enableVersioning({
    type: VersioningType.URI,
  });

  const document = SwaggerModule.createDocument(app, SWAGGER_CONFIG);
  SwaggerModule.setup('swagger', app, document);

  await app.listen(configuration().port, '0.0.0.0');
}

const logger = new Logger();

bootstrap()
  .then(() => logger.log(MESSAGES.APP_START))
  .catch((error) => logger.error(error));
