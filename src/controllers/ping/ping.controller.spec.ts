import { Test, TestingModule } from '@nestjs/testing';
import { PING_PONG_RESPONSE } from '../../constants';
import { PingService } from '../../services/ping/ping.service';
import { PingController } from './ping.controller';

describe('PingController', () => {
  let pingController: PingController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [PingController],
      providers: [PingService],
    }).compile();

    pingController = app.get<PingController>(PingController);
  });

  describe('root', () => {
    it(`should return ${PING_PONG_RESPONSE}`, () => {
      expect(pingController.pong().response).toBe(PING_PONG_RESPONSE);
    });
  });
});
