import { Controller, Get, Logger } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { PingService } from '../../services/ping/ping.service';
import { CONTROLLERS } from '../index';

/** ping контроллер. */
@Controller({ path: 'api' })
@ApiTags(CONTROLLERS.PING)
export class PingController {
  private readonly logger = new Logger(PingController.name);

  constructor(private readonly pingService: PingService) {}

  private get response() {
    return this.pingService.pong();
  }

  @Get(CONTROLLERS.PING)
  public pong() {
    this.logger.log(`pong: ${JSON.stringify(this.response)}`);
    return this.response;
  }
}
