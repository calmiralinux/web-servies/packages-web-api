import { Module } from '@nestjs/common';
import { PingService } from '../../services/ping/ping.service';
import { PingController } from './ping.controller';

@Module({
  imports: [],
  controllers: [PingController],
  providers: [PingService],
})
export class PingControllerModule {}
