export const CONTROLLERS = {
  PING: 'ping',
  PORTS: {
    ROOT: 'ports',
    METADATA: 'metadata',
    PORT: 'port',
  },
};
