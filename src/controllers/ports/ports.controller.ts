import { CacheInterceptor } from '@nestjs/cache-manager';
import { Controller, Get, Param, UseInterceptors } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import {
  DEFAULT_BRANCH,
  PortsService,
} from '../../services/ports/ports.service';
import { CONTROLLERS } from '../index';

const PORTS_BASE_PREFIX = CONTROLLERS.PORTS.ROOT;

@Controller({ path: 'api/v1/' })
@ApiTags(PORTS_BASE_PREFIX)
@UseInterceptors(CacheInterceptor)
export class PortsController {
  constructor(private readonly portsService: PortsService) {}

  @Get(`${PORTS_BASE_PREFIX}/${CONTROLLERS.PORTS.METADATA}/:branch`)
  public async getMetadata(
    @Param('branch')
    branch: string = DEFAULT_BRANCH,
  ) {
    return await this.portsService.getMetadata(branch);
  }

  @Get(
    `${PORTS_BASE_PREFIX}/${CONTROLLERS.PORTS.METADATA}/:branch/:${CONTROLLERS.PORTS.PORT}`,
  )
  public async getPortInfo(
    @Param('branch')
    branch: string = DEFAULT_BRANCH,
    @Param(CONTROLLERS.PORTS.PORT)
    port: string,
  ) {
    return await this.portsService.getPortInfo(branch, port);
  }

  @Get(
    `${PORTS_BASE_PREFIX}/${CONTROLLERS.PORTS.METADATA}/:branch/:${CONTROLLERS.PORTS.PORT}/install`,
  )
  public async getPortInstallInfo(
    @Param('branch')
    branch: string = DEFAULT_BRANCH,
    @Param(CONTROLLERS.PORTS.PORT)
    port: string,
  ) {
    return await this.portsService.getPortInstallInfo(branch, port);
  }

  @Get(
    `${PORTS_BASE_PREFIX}/${CONTROLLERS.PORTS.METADATA}/:branch/:${CONTROLLERS.PORTS.PORT}/readme`,
  )
  public async getPortReadme(
    @Param('branch')
    branch: string = DEFAULT_BRANCH,
    @Param(CONTROLLERS.PORTS.PORT)
    port: string,
  ) {
    return await this.portsService.getPortReadme(branch, port);
  }

  @Get(`${PORTS_BASE_PREFIX}/list/:branch`)
  public async getPorts(
    @Param('branch')
    branch: string = DEFAULT_BRANCH,
  ) {
    return await this.portsService.getPorts(branch);
  }
}
