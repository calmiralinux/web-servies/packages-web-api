import { Module } from '@nestjs/common';
import { AxiosHttpClient } from '../../services/axios-http.client';
import { PortsService } from '../../services/ports/ports.service';
import { PortsController } from './ports.controller';

@Module({
  controllers: [PortsController],
  providers: [AxiosHttpClient, PortsService],
})
export class PortsControllerModule {}
