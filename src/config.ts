import { APP_FALLBACK_PORT, CONFIG_KEYS } from './constants';

export default () => ({
  port: parseInt(process.env[CONFIG_KEYS.APP_PORT]!) || APP_FALLBACK_PORT,
});
