import { CacheModule } from '@nestjs/cache-manager';
import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import configuration from './config';
import { PingControllerModule } from './controllers/ping/ping.controller.module';
import { PortsControllerModule } from './controllers/ports/ports.controller.module';

/** Главный модуль. */
@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      load: [configuration],
    }),
    CacheModule.register({
      isGlobal: true,
      ttl: 180, // seconds
      max: 32, // maximum number of items in cache
    }),
    PingControllerModule,
    PortsControllerModule,
    CacheModule.register(),
  ],
})
export class AppModule {}
